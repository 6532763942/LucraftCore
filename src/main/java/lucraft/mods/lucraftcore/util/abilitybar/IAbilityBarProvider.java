package lucraft.mods.lucraftcore.util.abilitybar;

import java.util.List;

public interface IAbilityBarProvider {

    List<IAbilityBarEntry> getEntries();

}
